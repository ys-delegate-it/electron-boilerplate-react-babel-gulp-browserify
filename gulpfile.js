const gulp = require('gulp');
const uglify = require('gulp-uglify');
const browserify = require('browserify');
const watchify = require('watchify');
const babelify = require('babelify');
const sourcemaps = require('gulp-sourcemaps');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer')
const streamify = require('gulp-streamify');
const exit = require('gulp-exit');
const childProcess = require('child_process');
const path = require('path');
const less = require('gulp-less');
const minifyCSS = require('gulp-minify-css');
const rename = require('gulp-rename');
const plumber = require('gulp-plumber');

let electronStopping = false;
let electronProcess = null;

// Source and target LESS files
const cssMainFile = './public/less/app.less';
const cssFiles = './public/less/**/*.less';

gulp.task('buildjs', () => {
    return compile(false);
});
gulp.task('watchjs', () => {
    return watch();
});
gulp.task('watchcss', function() {
    gulp.watch(cssFiles, ['buildcss', 'buildfontawesome']);
});
gulp.task('run', () => {
    return runElectron();
});
gulp.task('buildcss', function() {
    return gulp.src(cssMainFile)
        .pipe(plumber())
        .pipe(less())
        .pipe(minifyCSS({ keepBreaks: false }))
        .pipe(rename({
            basename: "bundle",
            extname: ".min.css"
        }))
        .pipe(gulp.dest('./public/css'));
});
gulp.task('buildfontawesome', () => {
    return Promise.all([
        gulp.src('./bower_components/font-awesome/css/*.min.css')
            .pipe(gulp.dest('./public/css')),
        gulp.src('./bower_components/font-awesome/fonts/*.*')
            .pipe(gulp.dest('./public/fonts'))
    ]);
});
gulp.task('buildjquery', () => {
    return Promise.all([
        gulp.src('./bower_components/jquery/dist/jquery.min.js')
            .pipe(gulp.dest('./public/js')),
    ]);
});
gulp.task('start', ['build', 'watchjs', 'watchcss', 'run']);
gulp.task('build', ['buildcss', 'buildjs', 'buildfontawesome', 'buildjquery']);

function runElectron() {
    electronStopping = false;
    electronProcess = null;
    let electronPath = "./node_modules/electron/dist/electron";
    let electronParameters = ["main.js"];
    if (process.platform === 'darwin') {
        electronPath = `${electronPath}.app/Contents/MacOS/Electron`;
    }

    function startElectron() {
        electronProcess = childProcess.spawn(electronPath, electronParameters);
        electronProcess.stdout.pipe(process.stdout);
        electronProcess.on('close', (code, signal) => {
            electronStopping = true;
            console.log(' --> electron stopped...');
            console.log(`  .....code: '${code}'`);
            console.log(`  ...signal: '${signal}'`);
            electronProcess.kill();
        });
    }

    return startElectron();
}
function watch() {
    return compile(true);
}
function compile(requireWatch) {
    let bundler = browserify('./app/app.js', { debug: true })
        .transform(babelify, { presets: ["es2015", "react"] });

    function rebundle() {
        return bundler.bundle()
            .pipe(plumber())
            .on('error', err => {
                console.error(err);
                this.emit('end');
            })
            .pipe(source('bundle.min.js'))
            .pipe(buffer())
            .pipe(streamify(uglify()))
            .pipe(sourcemaps.init({ loadMaps: true }))
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest('./public/js'));
    }

    if (requireWatch) {
        bundler = watchify(bundler);
        bundler.on('update', () => {
            console.log(' --> bundling js...');
            rebundle();
        });
        let interval = 100;
        let timer = setInterval(() => {
            if (electronProcess !== null) {
                console.log(' --> electron process found, hooking on close event...');
                electronProcess.on('close', (code, signal) => {
                    console.log(' --> trying to stop bundling js...');
                    return gulp.src('.').pipe(exit());
                });
                console.log(' --> electron process hooked on close event, killing timer...');
                clearInterval(timer);
            }
            if (interval === 100) {
                interval = 1000;
            }
        }, interval);
    }

    return rebundle();
}