'use strict';

// Import React and ReactDOM
import React from 'react';
import ReactDOM from 'react-dom';

// Import AppContainer
import AppContainer from './containers/app.container';

// Hello world component
class App extends React.Component {
    static get propTypes() {
        return {
            audience: React.PropTypes.string
        };
    }
    render() {
        return (
            <AppContainer />
        );
    }
}

// Render to ID content in the DOM
ReactDOM.render(<App />, document.getElementById('react-container'));