'use strict';

import React, { Component } from 'react';
import md5 from 'md5';
const GRAVATAR_URL = "http://gravatar.com/avatar";

export default class Gravatar extends Component {
    static get propTypes() {
        return {
            email: React.PropTypes.string.isRequired,
            size: React.PropTypes.number
        };
    }
    render() {
        const size = this.props.size !== undefined ? this.props.size : 36 * 2;
        const hash = md5(this.props.email);
        const url = `${GRAVATAR_URL}/${hash}?s=${size}`;
        return (
            <div id='gravatar'>
                <img id='gravatarImage' className="gravatar" src={url} width={size} />
            </div>
        );
    }
}