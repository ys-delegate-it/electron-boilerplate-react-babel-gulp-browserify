'use strict';

// Import React
import React from 'react';

// Hello world component
export default class Hello extends React.Component {
    static get propTypes() {
        return {
            audience: React.PropTypes.string
        };
    }
    render() {
        return (
            <div>
                <h1>Hello {this.props.audience}!</h1>
            </div>
        );
    }
}