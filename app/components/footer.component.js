'use strict';

// Import React
import React from 'react';
import Gravatar from './gravatar.component';

// Footer component
export default class Footer extends React.Component {
    static get propTypes() {
        return {
            author: React.PropTypes.shape({
                name: React.PropTypes.string,
                email: React.PropTypes.string
            }).isRequired
        };
    }
    render() {
        return (
            <footer id="footer">
                <Gravatar email={this.props.author.email} />
                <div>C<i className="fa fa-code fa-inline"></i>ded by <a href={`mailto:${this.props.author.email}`}>{this.props.author.name}</a> With <i className="fa fa-heart"></i></div>
            </footer>
        );
    }
}