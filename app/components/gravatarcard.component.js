'use strict';

import React, { Component } from 'react';
import Gravatar from './gravatar.component';

export default class GravatarCard extends React.Component {
    static get propTypes() {
        return {
            name: React.PropTypes.string,
            email: React.PropTypes.string.isRequired
        };
    }
    render() {
        return (
            <div className="gravatar gravatar-card">
                <Gravatar className="photo" email={this.props.email} />
                <div className="email"><i className="fa fa-envelope"></i> <a href={`mailto:${this.props.email}`}>{this.props.name === undefined ? this.props.email : this.props.name}</a></div>
            </div>
        );
    }
}