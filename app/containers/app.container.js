'use strict';

// React library
import React from 'react';

// Axios of Ajax
import Axios from 'axios';

// Import Hello Component
import Hello from '../components/helloworld.component';
// Import Footer Component
import Footer from '../components/footer.component';
// Import GravatarCard Component
import GravatarCard from '../components/gravatarcard.component';

const users = [
    { name: "Yves Schelpe", email: "ys@delegate-it.be" },
    { name: "Dries De Beukelaer", email: "dries.debeukelaer@kdg.be" },
    { name: "Nadia Curinckx", email: "nadia_curinckx@kdg.be" },
    { name: "Dave De Kerf", email: "dave.dekerf@kdg.be" },
    { email: "bernard.vanisacker@gmail.com" },
    { name: "Marieke Lightband", email: "mariekelightband@gmail.com" },
    { name: "Ben Van de Cruys", email: "ben.vandecruys@gmail.com" },
    { name: "Zeke Sikelianos", email: "zeke@sikelianos.com" },
    { name: "Phil Haack", email: "haacked@gmail.com" },
    { email: "foo@bar.com" },
    { email: "ellia.bisker@gmail.com" },
    { email: "kevinwijckmans@gmail.com" },
    { email: "m@wxlr.mx" },
    { email: "marikattman@live.com" },
    { email: "mieke.hoing@kdg.be" },
    { email: "daveslounge@gmail.com" },
    { email: "koen@vulnerable.be" },
    { email: "peter.ronsmans@gmail.com" },
    { email: "info@psyaviah.com" }
];
const author = users[0];

// AppContainer
export default class AppContainer extends React.Component {
    // Constructor
    constructor(props) {
        super(props);
    }

    // Called once a component is loaded    
    componentDidMount() {
        console.log(`Component 'AppContainer' mounted.`);
    }

    // Render
    render() {
        const gravatarCards = users.sort((a, b) => {
            return a.email.localeCompare(b.email);
        }).map(user => {
            return <GravatarCard key={user.email} name={user.name} email={user.email} />
        });

        return (
            <div id="react-appcontainer">
                <div id='header'>
                    <Hello audience="People of Electron" />
                    <img className="svg" src="./public/img/logo.svg" />
                </div>
                <div id='gravatars'>
                    {gravatarCards}
                </div>
                <Footer author={author} />
            </div>
        );
    }
}