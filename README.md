Electron BOILERPLATE with React+Babel+Gulp+Browserify+etc..
===================
| **DISCLAIMER: Yes, I am aware of it!** | maintained by |
|-----|---|
| This is a fairly ***opiniated boilerplate*** for GitHub's **Electron**. Just **Clone** me via `git clone https://gitlab.com/ys-delegate-it/electron-boilerplate-react-babel-gulp-browserify.git/` and start editing it **your own way**! Everything you need to get started is included via the `package.json` and the `gulpfile.js`. You do however need npm bower globally installed to ensure a nice experience. |![Electron BOILERPLATE with React+Babel+Gulp+Browserify by ys@delegate-it.be](https://gitlab.com/uploads/project/avatar/1953805/delegateit-electron.jpg) [ys@delegate-it.be](mailto:ys@delegate-it.be) |


----------


Commands
-------------

#### NPM

> - Run `npm install` in the root dir when you launch it for the first time.
> - Then run `bower install` in the root dir to get the bower components ready.
> - After that it's time to hit start and run `npm start` in the root dir which on it's turn will run `gulp start`.

#### GULP

> **GULP Commands:**

> - Run `gulp start` in the root dir to start the electron app and edit as you go. Whenever you make a change to a .html / .less / .css file or a .js file gulp (with watchify and browserify) will automatically recompile everything and make it available in the app. The electron-reload package makes sure that upon those recompilings or file changes you can code & see your results directly in the app.
> - Run `gulp build` in the root dir to just build the app (e.g. before you want to package.

Packaging
-------------

> **Packaging is handled via the `electron-packager` package.**
 
> - Run `gulp build` to precompile everything one last time.
> - Run `electron-packager ./ --asar --out=./dist --archx64 --platform=win32 --version=1.4.5` in the root dir. This will build a 64bit architecture electron app based on electron 1.4.5 for windows in the dist directory.
